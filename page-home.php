<?php 

$single_content = get_field('single_content');

get_template_part("header-home"); // This fxn gets the header.php file and renders it ?>
	<div id="primary" class="row-fluid">
		<div class = "carousel-container">
			<div class = "grid-carousel">
				<input type = "radio" name = "index-buttons" id = "index1" checked>
				<label for = "index1"></label>
				<div class = "carousel-item hero" id = "item1"></div>
			</div>
		</div>

		<?php
			// Emits hero images repeater into a js array, then inits the carousel
			$images = array();
			while( have_rows('hero_carousel_images') ): the_row();
				$img = get_sub_field('image');
				array_push($images, $img);
			endwhile; 
		?>
		<script type="text/javascript">
			var heroImages = <?php echo '["' . implode('", "', $images) . '"]' ?>;
			jQuery(document).ready(function() { initCarousel(heroImages); });
		</script>

		<?php if($single_content): ?>

			<div class = "page-container">
				<div class = "block"><?php echo $single_content; ?></div>
				<hr>
			</div>

		<?php endif; ?>

		<div class = "article-container">
			<div class = "block1 block-container">
				<a href = "/wordpress/attractions">
					<div class = "block-content">
						<div class = "block-header">
							<h1>Attractions</h1>
						</div>
					</div>
				</a>
			</div>
			<div class = "block2 block-container">
				<a href = "/wordpress/about">
					<div class = "block-content">
						<div class = "block-header">
							<h1>About</h1>
						</div>
					</div>
				</a>
			</div>
			<div class = "block3 block-container">
				<a href = "/wordpress/location">
					<div class = "block-content">
						<div class = "block-header">
							<h1>Location</h1>
						</div>
					</div>
				</a>
			</div>
			<div class = "block4 block-container">
				<a href = "/wordpress/fishermans-suites">
					<div class = "block-content">
						<div class = "block-header">
							<h1>Fisherman's Suites</h1>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div><!-- #primary .content-area -->
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>