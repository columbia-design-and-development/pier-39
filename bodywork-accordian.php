<?php 
/**
 * 	Template Name: Accordian Bodyworks
 *
 *	This page template has a sidebar built into it, 
 * 	and can be used as a home page, in which case the title will not show up.
 *
*/

$block1 = get_field('swedish_comfort');
$block2 = get_field('breadwinner');
$block3 = get_field('matriarch/patriarch');
$block4 = get_field('the_whole_tribe');
$block5 = get_field('relaxibis');
$block6 = get_field('salty_dog');
$block7 = get_field('everyday_athlete');

get_header(); // This fxn gets the header.php file and renders it ?>
<div id="primary" class="row-fluid">
	<div id="content" role="main" class="span12">
        <div class = "accordian">
            <input type="checkbox" id="tab1" name="tabs">
            <label for="tab1">
				<h1>Swedish Comfort</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "tab1">
                    <?php echo $block1 ?>
                </div>
            </div>
            <input type="checkbox" id="tab2" name="tabs">
            <label for="tab2">
				<h1>Breadwinner</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "tab2">
                    <?php echo $block2 ?>
                </div>
            </div>
            <input type="checkbox" id="tab3" name="tabs">
            <label for="tab3">
                <h1>Matriarch/Patriarch</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "tab3">
                    <?php echo $block3 ?>
                </div>
            </div>
            <input type="checkbox" id="tab4" name="tabs">
            <label for="tab4">
                <h1>The Whole Tribe</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "tab4">
                    <?php echo $block4 ?>
                </div>
            </div>
            <input type="checkbox" id="tab5" name="tabs">
            <label for="tab5">
                <h1>Relaxibis</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "tab5">
                    <?php echo $block5 ?>
                </div>
            </div>
            <input type="checkbox" id="tab6" name="tabs">
            <label for="tab6">
                <h1>Salty Dog</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "tab6">
                    <?php echo $block6 ?>
                </div>
            </div>
            <input type="checkbox" id="tab7" name="tabs">
            <label for="tab7">
                <h1>Everyday Athlete</h1>
            </label>
            <div class="content">
                <div class = "the-content" id = "tab7">
                    <?php echo $block7 ?>
                </div>
            </div>
        </div>

    </div>
</div>


<?php get_footer(); ?>