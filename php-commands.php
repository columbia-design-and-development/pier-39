<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); // Display the user-defined menu in Appearance > Menus ?>
<a href="<?php echo esc_url( home_url( '/' ) ); // Link to the home page ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); // Title it with the blog name ?>" rel="home"><?php bloginfo( 'name' ); // Display the blog name ?></a>
<?php bloginfo( 'description' ); // Display the blog description, found in General Settings ?>
<?php bloginfo( 'title' ) ?>
<?php get_sidebar(); // This will display whatever we have written in the sidebar.php file, according to admin widget settings ?>
<?php get_template_part('home-header');?>
<?php echo get_stylesheet_directory_uri();// follow with file path from theme directory to reference file ?>
