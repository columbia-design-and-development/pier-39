<?php

$block1 = get_field('block1');
$single_content = get_field('single_content');
$map_link = get_field('map_link');

get_header(); // This fxn gets the header.php file and renders it ?>
	<div id="primary" class="row-fluid">

		<div class = "article-container">

		<?php if($single_content): ?>

			<div class = "page-container">
				<div class = "block"><?php echo $single_content; ?></div>
				<hr>
			</div>

		<?php endif; ?>

		<?php if($map_link): ?>
			<iframe 
				class="map-link"
				src="<?php echo $map_link; ?>" 
				allowfullscreen>
			</iframe>
		<?php endif; ?>

			<?php while( have_rows('page_content') ): the_row(); 

				$block_title = get_sub_field('block_title');
				$block_content = get_sub_field('block_content');
				$listing_id = get_sub_field('listing_id');
				$listing_title = get_sub_field('listing_title');
				$logo = get_sub_field('logo');
				$background = get_sub_field('background');
				$link = get_sub_field('link');
				$title = get_sub_field('title');
				$title_color = get_sub_field('title_color');

				?>

				<?php if ($block_content): ?>

					<div class = "block-container">
						<h1 class = "block-title"><?php echo $block_title; ?></h1>
						<hr>
						<div class = "block"><?php echo $block_content; ?></div>
						<hr>
					</div>
				
				<?php endif; ?>	

				<?php if ($listing_id): ?>

					<div class="airbnb-embed-frame" data-id="<?php echo $listing_id ?>" data-view="home" ><a href="https://www.airbnb.com/rooms/<?php echo $listing_id ?>?s=51"><span>View On Airbnb</span></a><a href="https://www.airbnb.com/rooms/<?php echo $listing_id ?>?s=51" rel="nofollow"><?php echo listing_title ?></a><script async="" src="https://www.airbnb.com/embeddable/airbnb_jssdk"></script></div>

				<?php endif; ?>

				<?php if ($logo): ?>
					<a class="logo-container" target="_blank" href="<?php echo $link ?>" style="background-color:<?php echo $background ?>">
						<div class="logo-img" style="background-image: url(<?php echo $logo ?>)"></div>
						<h3 style="color:<?php echo $title_color ?>"><?php echo $title ?></h3>
					</a>
				<?php endif; ?>					

			<?php endwhile; ?>

		
		</div><!-- article-container -->


	</div><!-- #primary .content-area -->
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>