<?php
	/*-----------------------------------------------------------------------------------*/
	/* This template will be called by all other template files to finish 
	/* rendering the page and display the footer area/content
	/*-----------------------------------------------------------------------------------*/
?>

	</main><!-- / end page container, begun in the header -->

	<footer class="site-footer">
		<div class = "social-icon-container">
			<?php get_sidebar();?>
		</div>
		<hr>
		<div class="site-info container">
			
			<div class = "footer-nav-container">
				<?php wp_nav_menu( array( 'theme_location' => 'primary' ) );?>
			</div>
			<p>Copyright, NBSD, LLC 2002-<?php echo date("Y") ?></p>
			<p>Designed, Developed by Columbia Design and Development</p>

			
		</div><!-- .site-info -->
	</footer><!-- #colophon .site-footer -->

</div><!-- end main-container -->
<script>
	jQuery(document).ready(function() {
		jQuery(".hamburger-icon").click(function() {
			jQuery(".menu-container").addClass("open");
			jQuery(".main-cover").addClass("fade-in");
			jQuery(".hamburger-helper").addClass("fade-out");
		});	
		jQuery(".close-button, .main-cover").click(function() {
			jQuery(".menu-container").removeClass("open");
			jQuery(".main-cover").removeClass("fade-in");
			jQuery(".hamburger-helper").removeClass("fade-out");
		});

		jQuery(window).scroll(function() {
			var windowTop = jQuery(this).scrollTop();
			var contentHeight = jQuery(".hamburger-helper").height();
			var headerHeight = jQuery(".site-header").height();

			//console.log(windowTop + " " + contentHeight);
			if (windowTop > contentHeight + headerHeight/2) { 
				jQuery(".hamburger-helper").addClass("drop-shadow");
				//jQuery(".block-container > *, .logo-container, .block-content").addClass("fade-up");
				jQuery(".block-container > *, .block-content").addClass("fade-up");
			} 
			else if(windowTop < contentHeight + headerHeight/2) {
				jQuery(".hamburger-helper").removeClass("drop-shadow");
				//jQuery(".block-container > *, .logo-container, .block-content").removeClass("fade-up");
				jQuery(".block-container > *, .block-content").removeClass("fade-up");
			}
		}).scroll();
    });
</script>

<?php wp_footer(); 
// This fxn allows plugins to insert themselves/scripts/css/files (right here) into the footer of your website. 
// Removing this fxn call will disable all kinds of plugins. 
// Move it if you like, but keep it around.
?>

</body>
</html>
