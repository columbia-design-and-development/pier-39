function initCarousel(heroImages) {
    var $home = jQuery(".cssgrid .home");
    if ($home.length) {
        console.log("home - supports grid");

        if (heroImages.length) {
            var imgCount = heroImages.length;
            for (var i = 0; i < heroImages.length; i++) {
                var img = heroImages[i];
                jQuery('<img/>').attr('src', img).on('load', function() {
                    jQuery(this).remove();
                    imgCount--;
                });
            }

            let intervalId = setInterval(function() { loadCarousel(imgCount, heroImages, intervalId ); }, 200);
        }
    }
}

function loadCarousel(imgCount, heroImages, intervalId) {
    if (imgCount === 0) {
        clearInterval(intervalId);
        var colCount = heroImages.length + 5; // 2 col padding on either side

        $carousel = jQuery(".carousel-container .grid-carousel");
        $carousel.css("grid-template-columns", "repeat(" + colCount + ", " + 100/colCount + "%)");

        if ($carousel.length) {
            for (var i = 0; i < heroImages.length; i++) {
                $carousel.append('<input type="radio" name="index-buttons" id="index' + (i+2) + '">');
                $carousel.append('<label for="index' + (i+2) + '" style="grid-column:' + (i+4) + '"></label>');
                $carousel.append('<div class="carousel-item hero" id="item' + (i+2) + '" style="background-image:url(' + heroImages[i] + ')">');
            }
        }
        cycleCarousel(heroImages);
    }
}

function cycleCarousel(heroImages) {
    let index = 1;
    let slideCount = heroImages.length + 1;
    let rotateDuration = 3000;

    function rotate() {
        index++;
        if (index > slideCount) {
            index = 1;
        }
        jQuery("#index"+index).prop("checked", true);
    }
    let carouselInterval = setInterval(rotate, rotateDuration);

    jQuery("label").click(function() {
        clearInterval(carouselInterval);
    });
}