<?php

add_action('admin_head', 'hide_default_editor');

function hide_default_editor() {
  echo '<style>
    .wp-admin .postarea { display: none !important; } 

    @media only screen and (max-width: 850px) {
      #post-body {display: grid; }
      #post-body-content { grid-row-start: 1}
      #postbox-container-2 { grid-row-start: 2}
    }
  </style>';
}
/*-----------------------------------------------------------------------------------*/
/* Activate sidebar for Wordpress use
/*-----------------------------------------------------------------------------------*/
function naked_register_sidebars() {
	register_sidebar(array(				// Start a series of sidebars to register
		'id' => 'sidebar', 					// Make an ID
		'name' => 'Sidebar',				// Name it
		'description' => 'Take it on the side...', // Dumb description for the admin side
		'before_widget' => '<div>',	// What to display before each widget
		'after_widget' => '</div>',	// What to display following each widget
		'before_title' => '<h3 class="side-title">',	// What to display before each widget's title
		'after_title' => '</h3>',		// What to display following each widget's title
		'empty_title'=> '',					// What to display in the case of no title defined for a widget
		// Copy and paste the lines above right here if you want to make another sidebar, 
		// just change the values of id and name to another word/name
	));
} 
// adding sidebars to Wordpress (these are created in functions.php)
add_action( 'widgets_init', 'naked_register_sidebars' );

/*-----------------------------------------------------------------------------------*/
/* Enqueue Styles and Scripts
/*-----------------------------------------------------------------------------------*/

function extra_scripts()  { 

	// get the theme directory style.css and link to it in the header
	wp_enqueue_style('style.css', get_stylesheet_directory_uri() . '/style.css');

	// add fitvid
	//wp_enqueue_script( 'naked-fitvid', get_template_directory_uri() . '/js/jquery.fitvids.js', array('jquery'), 1.0, true );

	// add theme scripts
	wp_enqueue_script( 'theme', get_template_directory_uri() . '/js/theme.js', array(), 1.0, true );
	wp_enqueue_script( 'carousel', get_template_directory_uri() . '/js/carousel.js', array(), 1.0, true );
}

add_action( 'wp_enqueue_scripts', 'extra_scripts' ); // Register this fxn and allow Wordpress to call it automatcally in the header
?>
